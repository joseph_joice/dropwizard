/**
 * Created by joseph on 16/4/15.
 */

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;


public class HelloWorldApplication extends Application<Hello> {
    public static void main(String[] args) throws Exception {
        new HelloWorldApplication().run(args);
    }

    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    public void initialize(Bootstrap<Hello> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(Hello configuration,
                    Environment environment) {
        // nothing to do yet
    }

}
